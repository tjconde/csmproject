# CMS Project

Internal web app to show list of properties for rent with following details. Also needs to have a button to edit each field, and a button to toggle between authorising and deauthorising the listing.
 
 
	LISTING PAGE
	Property 1
Firebase ID of property
	Running counter (e.g. #1, #23…)
	Image 1
	Address + Postal Code				Rent	
	Name of owner / agent				Firebase ID of owner/agent
<button to edit>
 
	Property 2
	Property 3
	…
	
 
	EDIT LISTING PAGE
	Rent <edit>
	Address + Postal code <edit>
	View Photo 1 <upload> <remove> *must check dimensions and image format*
	View Photo 2 <upload> <remove>
	View Photo 3 <upload> <remove>
	View Photo 4 <upload> <remove>
	View Photo 5 <upload> <remove>
	Facilities in HTML table
		Utilities Included <checkbox> *tick if current listing has this selected*
		WiFi included <checkbox>...
		…
	Duration of lease <dropdown - 6-12 months, 13-24 months, flexible> *pre-populated*
	Type of property <dropdown - HDB, Condo, Landed> *pre-populated*
	Type of rental <dropdown - Common Room, Master Room, Whole Unit> *pre-populated*
	
	Name of Owner / Agent <edit>
Phone Number <edit>
	Email <edit>
	
	<Authorise/Deauthorise button>
	If already authorised, show button marked as <Deauthorise property> and toggle listing to deauthorised when selected
	If unauthorised, show button marked as <Authorise property> and toggle listing to authorised when selected